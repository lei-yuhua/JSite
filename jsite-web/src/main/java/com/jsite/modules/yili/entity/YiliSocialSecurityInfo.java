/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jsite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

import java.util.Date;

/**
 * 伊利员工社保信息表Entity
 * @author liuruijun
 * @version 2020-07-29
 */
public class YiliSocialSecurityInfo extends DataEntity<YiliSocialSecurityInfo> {
	
	private static final long serialVersionUID = 1L;
	private String employeeId;		// 员工编号（外键） 父类
	private String employeeName;
	private String type;		// 社保类型
	private Date paymentPeriod;		// 缴费期间
	private double paymentAmount;		// 个人缴费金额
	
	public YiliSocialSecurityInfo() {
		super();
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public YiliSocialSecurityInfo(String id){
		super(id);
	}

	public YiliSocialSecurityInfo(YiliEmployee employeeId){
		this.employeeId = employeeId.getId();
	}

	@Length(min=1, max=64, message="员工编号（外键）长度必须介于 1 和 64 之间")
	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(YiliEmployee employeeId) {
		this.employeeId = employeeId.getId();
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	@Length(min=0, max=1, message="社保类型长度必须介于 0 和 1 之间")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@JsonFormat(pattern = "yyyy-MM")
	public Date getPaymentPeriod() {
		return paymentPeriod;
	}

	public void setPaymentPeriod(Date paymentPeriod) {
		this.paymentPeriod = paymentPeriod;
	}
	
	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	
}