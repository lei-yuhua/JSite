package com.jsite.modules.flowable.entity;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.DataEntity;

import java.io.IOException;
import java.io.Serializable;

public class TaskNode extends DataEntity<TaskNode> {
    private static final long serialVersionUID = 1L;

    private String modelId;			// 流程模型id
    private String modelKey;		// 流程模型key
    private String modelVersion;		// 流程模型版本

    private String fieldName;
    private String resourceId;
    private String resourceName;
    private Properties properties;
    private Stencil stencil;

    private String varName;

    private String isRead;
    private String isHide;
    private String isRequire;

    public TaskNode() {
        super();
    }

    public TaskNode(String modelKey, String modelVersion) {
        super();
        this.modelKey = modelKey;
        this.modelVersion = modelVersion;
    }

    public TaskNode(TaskNode node) {
        super();
        this.modelKey = node.getModelKey();
        this.modelVersion = node.getModelVersion();
        this.modelId = node.getModelId();
        this.fieldName = node.getFieldName();
    }

    public TaskNode(String fieldName, String modelId, String modelKey, String modelVersion) {
        super();
        this.fieldName = fieldName;
        this.modelId = modelId;
        this.modelKey = modelKey;
        this.modelVersion = modelVersion;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelKey() {
        return modelKey;
    }

    public void setModelKey(String modelKey) {
        this.modelKey = modelKey;
    }

    public String getModelVersion() {
        return modelVersion;
    }

    public void setModelVersion(String modelVersion) {
        this.modelVersion = modelVersion;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public Stencil getStencil() {
        return stencil;
    }

    public void setStencil(Stencil stencil) {
        this.stencil = stencil;
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName = varName;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getIsHide() {
        return isHide;
    }

    public void setIsHide(String isHide) {
        this.isHide = isHide;
    }

    public String getIsRequire() {
        return isRequire;
    }

    public void setIsRequire(String isRequire) {
        this.isRequire = isRequire;
    }

    public static class Properties implements Serializable {
        private static final long serialVersionUID = 1L;

        private String overrideid;
        private String name;

        private Conditionsequenceflow conditionsequenceflow;

        public Properties() {}

        public String getOverrideid() {
            return overrideid;
        }

        public void setOverrideid(String overrideid) {
            this.overrideid = overrideid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Conditionsequenceflow getConditionsequenceflow() {
            return conditionsequenceflow;
        }

        public void setConditionsequenceflow(Conditionsequenceflow conditionsequenceflow) {
            this.conditionsequenceflow = conditionsequenceflow;
        }
    }

    public static class Stencil implements Serializable {
        private static final long serialVersionUID = 1L;
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

    public static class Conditionsequenceflow implements Serializable {
        private static final long serialVersionUID = 1L;
        private Expression expression;
        public Conditionsequenceflow() {}

        public Conditionsequenceflow(String json) {

            if (StringUtils.isEmpty(json)) {
                Expression expression = new Expression();
                expression.setStaticValue("");
                expression.setType("");
                setExpression(expression);
            } else {
                try {
                    Conditionsequenceflow flow = new ObjectMapper().readValue(json, Conditionsequenceflow.class);
                    setExpression(flow.getExpression());
                } catch (IOException e) {
//                    e.printStackTrace();

                    Expression expression = new Expression();
                    expression.setStaticValue(json);
                    expression.setType("static");
                    setExpression(expression);

                }
            }
        }

        public Expression getExpression() {
            return expression;
        }

        public void setExpression(Expression expression) {
            this.expression = expression;
        }
    }

    public static class Expression implements Serializable {
        private static final long serialVersionUID = 1L;
        private String type;
        private String staticValue;

        public Expression() {}

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStaticValue() {
            return staticValue;
        }

        public void setStaticValue(String staticValue) {
            this.staticValue = staticValue;
        }
    }

}
